from django.urls import path
from . import views

app_name = 'mktg_coll_designs'

urlpatterns = [
    path('', views.marketing, name='mktg_coll_designs'),
]
