from django.apps import AppConfig


class MktgCollDesignsConfig(AppConfig):
    name = 'mktg_coll_designs'
