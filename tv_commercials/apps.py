from django.apps import AppConfig


class TvCommercialsConfig(AppConfig):
    name = 'tv_commercials'
