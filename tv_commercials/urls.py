from django.urls import path
from . import views

app_name = 'tv_commercials'

urlpatterns = [
    path('', views.tv_commercials, name='tv_commercials'),
]
