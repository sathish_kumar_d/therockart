from django.shortcuts import render

# Create your views here.
def tv_commercials(request):
    return render(request, 'tv_commercials/tv_commercials.html')