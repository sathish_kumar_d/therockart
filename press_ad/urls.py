from django.urls import path
from . import views

app_name = 'press_ad'

urlpatterns = [
    path('', views.press_ad, name='press_ad'),
]
