from django.apps import AppConfig


class PressAdConfig(AppConfig):
    name = 'press_ad'
