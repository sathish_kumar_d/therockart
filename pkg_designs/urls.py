from django.urls import path
from . import views

app_name = 'pkg_designs'

urlpatterns = [
    path('', views.pkg_designs, name='pkg_designs'),
]
