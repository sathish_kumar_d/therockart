from django.shortcuts import render

# Create your views here.
def pkg_designs(request):
    return render(request, 'pkg_designs/pkg_designs.html')
