from django.apps import AppConfig


class PkgDesignsConfig(AppConfig):
    name = 'pkg_designs'
