from django.apps import AppConfig


class BrochureDesignsConfig(AppConfig):
    name = 'brochure_designs'
