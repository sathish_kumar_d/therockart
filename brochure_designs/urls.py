from django.urls import path
from . import views

app_name = 'brochure_designs'

urlpatterns = [
    path('', views.brochures, name='brochure_designs'),
]
