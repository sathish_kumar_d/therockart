from django.shortcuts import render, get_object_or_404

def brochures(request):
    return render(request, 'brochure_designs/brochure_designs.html')