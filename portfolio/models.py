from django.db import models

class Project(models.Model):
    title = models.CharField(max_length=100)
    page_url_name = models.CharField(max_length=50)
    description = models.CharField(max_length=250, blank=True)

    