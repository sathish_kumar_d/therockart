from django.shortcuts import render, get_object_or_404

def brands(request):
    return render(request, 'brand_identity/brand_iden.html')