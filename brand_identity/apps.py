from django.apps import AppConfig


class BrandIdentityConfig(AppConfig):
    name = 'brand_identity'
