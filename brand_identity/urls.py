from django.urls import path
from . import views

app_name = 'brand_identity'

urlpatterns = [
    path('', views.brands, name='brand_identity'),
]
