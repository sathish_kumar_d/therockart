from django.urls import path
from . import views

app_name = 'inshop_branding'

urlpatterns = [
    path('', views.inshop_branding, name='inshop_branding'),
]
