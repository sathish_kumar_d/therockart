from django.apps import AppConfig


class InshopBrandingConfig(AppConfig):
    name = 'inshop_branding'
