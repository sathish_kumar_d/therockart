"""therockart URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from portfolio import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name='home'),
    path('about', views.about, name='about'),
    path('brand_identity/', include('brand_identity.urls', namespace='brand_identity')),
    path('brochure_designs/', include('brochure_designs.urls', namespace='brochure_designs')),
    path('marketing_collateral_designs/', include('mktg_coll_designs.urls', namespace='mktg_coll_designs')),
    path('press_ad/', include('press_ad.urls', namespace='press_ad')),
    path('corporate_presentations/', include('corp_pres.urls', namespace='corp_pres')),
    path('tv_commercials/', include('tv_commercials.urls', namespace='tv_commercials')),
    path('packaging_designs/', include('pkg_designs.urls', namespace='pkg_designs')),
    path('inshop_branding/', include('inshop_branding.urls', namespace='inshop_branding')),
    path('web_designs/', include('web_designs.urls', namespace='web_designs')),
]
