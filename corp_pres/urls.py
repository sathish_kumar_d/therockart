from django.urls import path
from . import views

app_name = 'corp_pres'

urlpatterns = [
    path('', views.corp_pres, name='corp_pres'),
]
