from django.urls import path
from . import views

app_name = 'web_designs'

urlpatterns = [
    path('', views.web_designs, name='web_designs'),
]
