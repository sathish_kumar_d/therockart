from django.shortcuts import render

# Create your views here.
def web_designs(request):
    return render(request, 'web_designs/web_designs.html')