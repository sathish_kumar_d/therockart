from django.apps import AppConfig


class WebDesignsConfig(AppConfig):
    name = 'web_designs'
